package twm.com.demo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.example.loginfb.R;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class StartActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
		
		 try {
			 //Change to Your Package Info
		        PackageInfo info = getPackageManager().getPackageInfo(
		        		getPackageName(), 
		                PackageManager.GET_SIGNATURES);
		        for (Signature signature : info.signatures) {
		            MessageDigest md = MessageDigest.getInstance("SHA");
		            md.update(signature.toByteArray());
		            Log.i("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
		            }
		    } catch (NameNotFoundException e) {
		    	Log.i("KeyHash", "NameNotFoundException");
		    	e.printStackTrace();
		    } catch (NoSuchAlgorithmException e) {
		    	Log.i("KeyHash", "NoSuchAlgorithmException");
		    	e.printStackTrace();
		    }catch (Exception e) {
		    	Log.i("KeyHash", "Exception");
				e.printStackTrace();
			}	
		this.setContentView(R.layout.start);
		Toast.makeText(StartActivity.this, "start activity",
				Toast.LENGTH_LONG).show();
		Button btn = (Button) findViewById(R.id.hellobtn);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(StartActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
	}

}
