package twm.com.demo;



import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONObject;

import twm.com.tools.*;

import com.example.loginfb.R;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

	
	// FB權限設定
	String[] fb_Permissions = new String[] { "email", "publish_stream",
			"publish_actions" };
	GraphUser user = null;
	// 預設ＬＯＧ ＴＡＧ名稱
	String tag = "facebook";
	// 邀請好友ＢＵＴＴＯＮ
	Button sendInviteButton;
	Button getIVNumButton;

	// ＰＯ文訊息
	Button sendShareButton;
	// 按讚ＢＵＴＴＯＮ
	Button sendLikeButton;
	
	private Context context = this;
	private Activity activity = (Activity) context;
	
	// action type define
	private final int SENDSHARE = 1;
	private final int DOINVITE	= 2;
	private final int GETINVNB 	= 3;
	private final int DOLIKE 	= 4;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		HttpTask1 task = new HttpTask1();
		task.execute("");




		// 按讚Ｂutton初始化
		sendLikeButton = (Button) findViewById(R.id.send_like);

		sendLikeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FacebookUtil.GetInstance(context, MainActivity.this).PopLikeWindow();
			}

		});

		// 邀請好友ＦＢ上
		sendShareButton = (Button) findViewById(R.id.send_share);
		sendShareButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				//2014/08/21 Stanley add default post words
				try{
					JSONObject game_info = new JSONObject(GameInfoUtil.GetInstance(context, activity).GetGameInfo());
					String default_info=game_info.getString("FA_MESSAGE");
					FacebookUtil.GetInstance(context, MainActivity.this).SendShareWords(default_info);
				}catch(Exception e){
					e.printStackTrace();
				}
			}

		});


//		// 取得邀請好友數字
//		getIVNumButton = (Button) findViewById(R.id.getIVNumButton);
//		getIVNumButton.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				
//					String num = GameInfoUtil.GetInstance(context, activity).GetInviteNum();
//					if(num == null)
//					{
//						FacebookUtil.GetInstance(context, MainActivity.this).FacebookLogin();
//					}
//					else
//					{
//						Toast.makeText(activity,
//								"Your invite Number is " + num,
//								Toast.LENGTH_SHORT).show();
//					}
//				
//			}
//
//		});

		// 邀請好友Button
		sendInviteButton = (Button) findViewById(R.id.send_invite);
		// 送出邀請
		sendInviteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
					FacebookUtil.GetInstance(context, MainActivity.this).DoInvite();
			}

		});

	}
	
	@Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		    super.onActivityResult(requestCode, resultCode, data);
		    Session.getActiveSession().onActivityResult(this, requestCode,resultCode, data);
		}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	class HttpTask extends AsyncTask<Integer, Integer, String>
	{
		@Override
		protected String doInBackground(Integer... type) {
			// TODO Auto-generated method stub
			if(type.length == 1)
			{
				// TODO
				switch(type[0])
				{
				case DOLIKE:
					FacebookUtil.GetInstance(context, MainActivity.this).PopLikeWindow();
					break;
				case DOINVITE:
					FacebookUtil.GetInstance(context, MainActivity.this).DoInvite();
					break;
				case SENDSHARE:
					FacebookUtil.GetInstance(context, MainActivity.this).SendShareWords("");
					break;
				default:
					break;
				}
			}
			return null;
		}
		
	}
	
	
	class HttpTask1 extends AsyncTask<String, Integer, String>
	{
		@Override
		protected String doInBackground(String... type) {
			// TODO Auto-generated method stub
			GameInfoUtil.GetInstance(context, MainActivity.this).GetGameInfo();
			return null;
		}
		
	}

	
}
