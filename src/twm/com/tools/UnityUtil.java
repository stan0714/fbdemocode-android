package twm.com.tools;

import twm.com.demo.MainActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.unity3d.player.UnityPlayer;

public class UnityUtil {

	private final String TAG = "UnityUtil";
	
	public void DoShare(final String words)
	{
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				FacebookUtil.GetInstance(UnityPlayer.currentActivity, UnityPlayer.currentActivity).SendShareWords(words);
			}
		});
	}
	
	public void DoGetNum()
	{
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				// TODO Auto-generated method stub
				String num = GameInfoUtil.GetInstance(UnityPlayer.currentActivity, UnityPlayer.currentActivity).GetInviteNum();
				if(num == null)
				{
					FacebookUtil.GetInstance(UnityPlayer.currentActivity, UnityPlayer.currentActivity).FacebookLogin();
				}
			}
		});
	}
	
	public void DoInvite()
	{
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				FacebookUtil.GetInstance(UnityPlayer.currentActivity, UnityPlayer.currentActivity).DoInvite();
			}
		});
	}
	
	public void DoLike()
	{
		UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				FacebookUtil.GetInstance(UnityPlayer.currentActivity, UnityPlayer.currentActivity).PopLikeWindow();
			}
		});
	}
	
	public void InitGameInfo()
	{
		HttpTask task = new HttpTask();
		task.execute("");
	}
	
	class HttpTask extends AsyncTask<String, Integer, String>
	{
		@Override
		protected String doInBackground(String... type) {
			// TODO Auto-generated method stub
			GameInfoUtil.GetInstance(UnityPlayer.currentActivity, UnityPlayer.currentActivity).GetGameInfo();
			return null;
		}
		
	}
}
