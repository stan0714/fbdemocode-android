package twm.com.tools;

import java.io.ObjectOutputStream.PutField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.json.JSONException;
import org.json.JSONObject;

import twm.com.tools.GameInfoUtil.InviteNumTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Session.OpenRequest;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog.ShareDialogBuilder;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

public class FacebookUtil {

	private static FacebookUtil instance = null;

	private static Context context;

	private static Activity activity;

	private GraphUser graphUser;

	private final String tag = "FacebookUtil";

	private static final List<String> PERMISSIONS = Arrays
			.asList("publish_actions");

	/**
	 * sington method
	 * 
	 * @param ct
	 * @return
	 */
	public static FacebookUtil GetInstance(Context ct, Activity at) {
		if (instance == null) {
			instance = new FacebookUtil();
		}
		// context maybe different,set new context every time
		context = ct;
		activity = at;
		return instance;
	}

	/**
	 * get user,if not login,return would be null. make sure checksession is
	 * called before to avoid return null
	 * 
	 * @return
	 */
	public GraphUser GetUser() {
		return graphUser;
	}

	/**
	 * check session
	 * 
	 * @return if opened:true, else false
	 */
	public boolean CheckSession() {
		Session session = Session.getActiveSession();
		if (session == null) {
			return false;
		}

		return session.isOpened();
	}

	public void FacebookLogin() {
		if (Session.getActiveSession() != null) {
			Session session = Session.getActiveSession();

			Log.i(tag,
					"[FacebookLogin]session is in! Session is "
							+ session.getState()
							+ "  "
							+ session.getState().toString()
									.equals("CLOSED_LOGIN_FAILED"));
			// 2014/08/11 Stanley check session is login faild
			if (!session.getState().toString().equals("CLOSED_LOGIN_FAILED")) {
				// 2014/08/07 Stanley add openForPublish
				session.openForPublish(new OpenRequest(activity)
						.setPermissions(PERMISSIONS).setCallback(
								new Session.StatusCallback() {
									// 在過程中只要 Session 狀態改變, 這個 Callback 就會被呼叫
									@SuppressWarnings("deprecation")
									public void call(Session session,
											SessionState state,
											Exception exception) {
										if (session == null) {
											Log.i(tag,
													"[FacebookLogin]call session is null!");
											return;
										}
										Log.i(tag,
												"graphuser111111:"
														+ session.toString());
										if (session.isOpened()) {

											List<String> permissions = session
													.getPermissions();
											// for (int i = 0; i <
											// permissions.size(); i++) {
											// Log.i(tag,
											// "1:GOGO:"+permissions.get(i));
											// }
											// 2014/08/07 Stanley add
											// openForPublish
											if (!permissions
													.containsAll(PERMISSIONS)) {

												session.addCallback(this);
												session.requestNewPublishPermissions(new Session.NewPermissionsRequest(
														activity, PERMISSIONS));
												return;
											}

											Log.i(tag, "graphuser2222:"
													+ session.toString());
											Request.executeMeRequestAsync(
													session,
													new Request.GraphUserCallback() {
														@Override
														public void onCompleted(
																GraphUser user,
																Response response) {
															if (user != null) {
																graphUser = user;
																Toast.makeText(
																		context,
																		"Login success 111",
																		Toast.LENGTH_SHORT)
																		.show();
															} else {
																Log.i(tag,
																		"GraphUser[onCompleted] is null");
																Toast.makeText(
																		context,
																		"Login cancelled or failed",
																		Toast.LENGTH_SHORT)
																		.show();
															}
														}
													});
										}
									}
								}));

				return;
			}
		}
		Session.openActiveSession(activity, true, new Session.StatusCallback() {
			// 在過程中只要 Session 狀態改變, 這個 Callback 就會被呼叫
			@SuppressWarnings("deprecation")
			public void call(Session session, SessionState state,
					Exception exception) {
				if (session == null) {
					Log.i(tag, "[FacebookLogin]call session is null!");
					return;
				}
				Log.i(tag, "graphuser111111:" + session.toString());
				if (session.isOpened()) {
					// 2014/08/07 Stanley add openForPublish
					List<String> permissions = session.getPermissions();
					// for (int i = 0; i < permissions.size(); i++) {
					// Log.i(tag, "2:GOGO:"+permissions.get(i));
					// }
					//2014/08/26 Stanley Remark
					if (!permissions.containsAll(PERMISSIONS)) {

						session.addCallback(null);
						session.requestNewPublishPermissions(new Session.NewPermissionsRequest(
								activity, PERMISSIONS));
						return;
					}
					Log.i(tag, "graphuser:" + session.toString());
					Request.executeMeRequestAsync(session,
							new Request.GraphUserCallback() {
								@Override
								public void onCompleted(GraphUser user,
										Response response) {
									if (user != null) {
										graphUser = user;
										Toast.makeText(context,
												"Login success",
												Toast.LENGTH_SHORT).show();
									} else {
										Log.i(tag,
												"GraphUser[onCompleted] is null");
										Toast.makeText(context,
												"Login cancelled or failed",
												Toast.LENGTH_SHORT).show();
									}
								}
							});
				}
			}
		});
	}

	public void FacebookLogout() {
		Session session = Session.getActiveSession();
		if (session != null) {
			session.closeAndClearTokenInformation();
		}
		Session.setActiveSession(null);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void PopLikeWindow() {
		try {
			AlertDialog.Builder alert = new AlertDialog.Builder(context);
			JSONObject response = new JSONObject(GameInfoUtil.GetInstance(
					context, activity).GetGameInfo());
			alert.setTitle(response.getString("FA_LIKE_TITLE"));
			String url = response.getString("FA_LIKE_URL");
			// 2014/05/19 Stanley send like url
			setLikeUrl(url);
			LinearLayout wrapper = new LinearLayout(context);
			WebView wv = new WebView(context);
			EditText keyboardHack = new EditText(context);

			keyboardHack.setVisibility(View.GONE);
			wrapper.setOrientation(LinearLayout.VERTICAL);
			wrapper.addView(wv, LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			wrapper.addView(keyboardHack,
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			WebSettings webSettings = wv.getSettings();
			webSettings.setJavaScriptEnabled(true);
			wv.loadUrl(url);
			wv.setWebViewClient(new WebViewClient() {
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					Log.i("facebook", "shouldOverrideUrlLoading:" + url);
					view.loadUrl(url);

					return true;
				}

				// 2014/05/19 Stanley fix facebook login
				public void onPageFinished(WebView view, String url) {
					Log.i("facebook", "onPageFinished:" + url);
					// String redirectURL = new String();
					if (url.startsWith("https://www.facebook.com/connect/connect_to_external_page_widget_loggedin.php")) {
						view.loadUrl(likeUrl);
						return;
					} else if (url
							.startsWith("https://www.facebook.com/plugins/close_popup.php")) {
						Log.i("facebook", url + " : " + likeUrl);
						view.loadUrl(likeUrl);
						return;
					}
				}

			});

			alert.setView(wrapper);

			alert.setNegativeButton("Close",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
			alert.show();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i(tag, "pop likewindow exception!");
		}
	}

	// Facebook send like url
	String likeUrl;

	public String getLikeUrl() {
		return likeUrl;
	}

	public void setLikeUrl(String likeUrl) {
		this.likeUrl = likeUrl;
	}

	/**
	 * do invite friend
	 */
	public void DoInvite() {
		try {
			JSONObject game_info = new JSONObject(GameInfoUtil.GetInstance(
					context, activity).GetGameInfo());
			// build params
			Bundle params = new Bundle();
			params.putString("title", game_info.getString("FA_REQ_TITLE"));
			params.putString("message", game_info.getString("FA_REQ_MESSAGE"));
			params.putString("data", game_info.getString("FA_REQ_MESSAGE"));
			params.putString("filters", "app_non_users");
			params.putInt("max_recipients", 50);
			Session session = Session.getActiveSession();
			if (session == null || !session.isOpened()) {
				FacebookLogin();
			} else {
				// Facebook Send Request Dialog
				WebDialog requestsDialog = (new WebDialog.RequestsDialogBuilder(
						context, session, params)).setOnCompleteListener(
						new OnCompleteListener() {
							@SuppressWarnings("unchecked")
							@Override
							public void onComplete(Bundle values,
									FacebookException error) {
								if (values == null) {
									Log.i(tag, "onComlete bundle is null!");
									return;
								}
								String inviteID = "";
								// 取得邀請人數
								ArrayList<String> id = new ArrayList<String>();
								for (String key : values.keySet()) {
									if (key.indexOf("to[") != -1) {
										id.add(values.getString(key));
										inviteID += values.get(key) + ",";
									}
									String temp = StringUtils.join(
											id.toArray(), ",");
									Log.i("facebook", " " + key + " => "
											+ values.get(key) + ";" + " :"
											+ temp);
								}
								final String requestId = values
										.getString("request");
								if (requestId != null) {
									String token = Session.getActiveSession()
											.getAccessToken();
									Log.i(tag, "ID:" + inviteID + " TOKEN:"
											+ token);
									// HTTP POST MAP
									Map<String, String> postParams = new HashMap<String, String>();
									postParams.put("TOKEN", token);
									postParams.put("FBID", graphUser.getId());
									postParams.put("IVValue", inviteID);
									postParams.put(
											"FA_ID",
											GameInfoUtil
													.GetInstance(context,
															activity)
													.GetProperties()
													.getProperty("FA_ID"));

									Map<String, Map<String, String>> urlmap = new HashMap<String, Map<String, String>>();
									urlmap.put(
											GameInfoUtil
													.GetInstance(context,
															activity)
													.GetProperties()
													.getProperty(
															"game_insert_iv_url"),
											postParams);
									InviteTask task = new InviteTask();
									task.execute(urlmap);
									// HttpClientUtil
									// .getInstance()
									// .excePost(
									// GameInfoUtil.GetInstance(context,
									// activity).GetProperties()
									// .getProperty("game_insert_iv_url"),
									// postParams);
									Toast.makeText(context, "Request sent",
											Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(context,
											"Request cancelled",
											Toast.LENGTH_SHORT).show();
								}
							}

						}).build();
				requestsDialog.show();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	/**
	 * post the share words to facebook wall
	 * 
	 * @param words
	 */
	@SuppressWarnings("unchecked")
	public void PostToWall(String words) {
		try {
			if (CheckSession()) {
				Session session = Session.getActiveSession();

				// GET MYPLAY1 Share INFO
				JSONObject game_info = new JSONObject(GameInfoUtil.GetInstance(
						context, activity).GetGameInfo());
				// HTTP POST MAP
				Map<String, String> postParams = new HashMap<String, String>();
				// PO 文需要取得用戶TOKEN
				postParams.put("access_token", session.getAccessToken());
				// 遊戲分享設定
				postParams.put("name", game_info.getString("FA_NAME"));
				postParams.put("caption", game_info.getString("FA_CAPTION"));
				postParams.put("description", words);
				// 取得貼文資訊
				postParams.put("message", words);
				postParams.put("link", game_info.getString("FA_LINK"));
				postParams.put("picture", game_info.getString("FA_PICTURE"));
				Map<String, Map<String, String>> urlmap = new HashMap<String, Map<String, String>>();
				// /v2.0/me/friends
				String url = GameInfoUtil.GetInstance(context, activity)
						.GetProperties().getProperty("facevbook_graph_url");
				urlmap.put(url, postParams);
				PostWallTask task = new PostWallTask();
				task.execute(urlmap);
				// String repsone = HttpClientUtil.getInstance().excePost(
				// GameInfoUtil.GetInstance(context,
				// activity).GetProperties().getProperty("facevbook_graph_url"),
				// postParams,
				// "UTF-8");
				// Log.i(tag, "Facebook response:" + repsone);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * //2014/09/11 Stanley add newPostToWall
	 * post the share words to facebook wall
	 * 
	 * @param words
	 */
	@SuppressWarnings("unchecked")
	public void newPostToWall() {
		try {
			if (CheckSession()) {
				Session session = Session.getActiveSession();

				// GET MYPLAY1 Share INFO
				JSONObject game_info = new JSONObject(GameInfoUtil.GetInstance(
						context, activity).GetGameInfo());
				// HTTP POST MAP
				Bundle postParams = new Bundle();
				// PO 文需要取得用戶TOKEN
				//postParams.putString("access_token", session.getAccessToken());
				// 遊戲分享設定
				postParams.putString("name", game_info.getString("FA_NAME"));
				postParams.putString("caption", game_info.getString("FA_CAPTION"));
				postParams.putString("description", game_info.getString("FA_MESSAGE"));
				// 取得貼文資訊
				postParams.putString("message", game_info.getString("FA_MESSAGE"));
				postParams.putString("link", game_info.getString("FA_LINK"));
				postParams.putString("picture", game_info.getString("FA_PICTURE"));
				
		
				WebDialog feedDialog = (
				        new WebDialog.FeedDialogBuilder(activity,
				           session,
				           postParams))
				        .setOnCompleteListener(new OnCompleteListener() {

				            @Override
				            public void onComplete(Bundle values,
				                FacebookException error) {
				                if (error == null) {
				                    // When the story is posted, echo the success
				                    // and the post Id.
				                    final String postId = values.getString("post_id");
				                    if (postId != null) {
				                        Toast.makeText(activity,
				                            "Posted story, id: "+postId,
				                            Toast.LENGTH_SHORT).show();
				                    } else {
				                        // User clicked the Cancel button
				                        Toast.makeText(activity.getApplicationContext(), 
				                            "Publish cancelled", 
				                            Toast.LENGTH_SHORT).show();
				                    }
				                } else if (error instanceof FacebookOperationCanceledException) {
				                    // User clicked the "x" button
				                    Toast.makeText(activity.getApplicationContext(), 
				                        "Publish cancelled", 
				                        Toast.LENGTH_SHORT).show();
				                } else {
				                    // Generic, ex: network error
				                    Toast.makeText(activity.getApplicationContext(), 
				                        "Error posting story", 
				                        Toast.LENGTH_SHORT).show();
				                }
				            }

				        })
				        .build();
				    feedDialog.show();
				
				
				
				
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void SendShareWords(String words) {
		if (CheckSession()) {
			//2014/09/11 Stanley Change to newPostToWall
			newPostToWall();
		} else {
			Toast.makeText(context, "重新取得Session中..", Toast.LENGTH_SHORT)
					.show();
			FacebookLogin();
		}
	}

	class InviteTask extends
			AsyncTask<Map<String, Map<String, String>>, Integer, String> {

		@Override
		protected String doInBackground(
				Map<String, Map<String, String>>... params) {
			// TODO Auto-generated method stub
			if (params.length == 1) {
				Map<String, Map<String, String>> map = params[0];
				Set<String> keyset = map.keySet();
				Iterator<String> it = keyset.iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					Map<String, String> value = (Map<String, String>) map
							.get(key);
					HttpClientUtil.getInstance().excePost(key, value);
					break;
				}
			}
			return null;
		}

	}

	class PostWallTask extends
			AsyncTask<Map<String, Map<String, String>>, Integer, String> {

		private final static String encoding = "UTF-8";

		@Override
		protected String doInBackground(
				Map<String, Map<String, String>>... params) {
			// TODO Auto-generated method stub
			String response = "";
			if (params.length == 1) {
				Map<String, Map<String, String>> map = params[0];
				Set<String> keyset = map.keySet();
				Iterator<String> it = keyset.iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					Map<String, String> value = (Map<String, String>) map
							.get(key);
					Log.i(tag, "Facebook Key:" + key + " value:" + value);
					response = HttpClientUtil.getInstance().excePost(key,
							value, encoding);
					break;
				}
			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.i(tag, "Facebook response:" + result);
			if (result.equals("") || result == null) {
				Toast.makeText(activity, "Please Post later,too frequently!",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

}
