package twm.com.tools;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class GameInfoUtil {

	// propertyReader
	private AssetsPropertyReader assetsPropertyReader;
	// the instance of util
	private static GameInfoUtil instance = null;
	// property
	private Properties properties;
	// static property file name
	private final static String profile = "FBDemo.properties";
	
	private final static String tag = "GameInfoUtil";
	
	// the gameinfo json result
	private String gameinfoJson = "";
	
	private Context context;
	
	private Activity activity;
	
	/**
	 * construct method
	 */
	private GameInfoUtil(Context ct, Activity at)
	{
		context = ct;
		activity = at;
		// init propertyreader
		if(assetsPropertyReader == null)
		{
			assetsPropertyReader = new AssetsPropertyReader(context);
		}
		// init property
		if(properties == null)
		{
			properties = assetsPropertyReader.getProperties(profile);
		}
		gameinfoJson = "";
	}
	
	/**
	 * the sington method
	 */
	public static GameInfoUtil GetInstance(Context context, Activity at)
	{
		if(instance == null)
		{
			instance = new GameInfoUtil(context, at);
		}
		
		return instance;
	}
	
	/**
	 * 
	 * @return the JSON type of GameInfo
	 */
	public String GetGameInfo() {
		// avoid to get gameinfo by http every time
		if(gameinfoJson == "")
		{
			String uri =  properties.getProperty("game_info_url");
			Map<String, String> game_info_map = new HashMap<String, String>();
			game_info_map.put("groupid", properties.getProperty("groupid"));
			String respone = HttpClientUtil.getInstance().exceGet(uri,
					game_info_map);
			Log.i(tag, "response:" + respone);
			
			gameinfoJson = respone;
		}
		
		return gameinfoJson;
	}
	
	public Properties GetProperties()
	{
		return properties;
	}
	
	@SuppressWarnings("unchecked")
	public String GetInviteNum()
	{
		if (FacebookUtil.GetInstance(context, activity).CheckSession()) {
			// 取得用戶的邀請人數
			// HTTP GET MAP
			Map<String, String> getParams = new HashMap<String, String>();
			getParams.put("FBID", FacebookUtil.GetInstance(context, activity).GetUser().getId());
			getParams.put("FA_ID", properties.getProperty("FA_ID"));
			String url = properties.getProperty("game_get_iv_url");
			Log.i(tag, "FBID:" + FacebookUtil.GetInstance(context, activity).GetUser().getId());
			Log.i(tag, "FA_ID:" + properties.getProperty("FA_ID"));
			Log.i(tag, "url:" + url);
			Map<String, Map<String, String>> urlmap = new HashMap<String, Map<String,String>>();
			urlmap.put(url, getParams);
			InviteNumTask task = new InviteNumTask();
			task.execute(urlmap);
			Log.i(tag, "get info success!");
			return "success";
		}
		// session is null or exception occured
		return null;
	}
	
	class InviteNumTask extends AsyncTask<Map<String, Map<String, String>>, Integer, String>
	{
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			JSONObject object;
			try {
				object = new JSONObject(result);
				String invite_number = object.getString("number");
				Toast.makeText(activity,
						"Your invite Number is " + invite_number,
						Toast.LENGTH_SHORT).show();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		protected String doInBackground(
				Map<String, Map<String, String>>... params) {
			// TODO Auto-generated method stub
			String result = "";
			if(params.length == 1)
			{
				Map<String, Map<String, String>> map = params[0];
				Set<String> keyset = map.keySet();
				Iterator<String> it = keyset.iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					Map<String, String> value = (Map<String, String>) map.get(key);
					result = HttpClientUtil.getInstance().exceGet(key, value);
					break;
				}
			}
			return result;
		}}
	
}
